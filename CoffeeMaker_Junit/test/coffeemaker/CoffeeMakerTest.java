package coffeemaker;

import coffeemaker.exceptions.InventoryException;
import coffeemaker.exceptions.RecipeException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Sarah Heckman
 *
 * Unit tests for CoffeeMaker class.
 */
public class CoffeeMakerTest {

    public CoffeeMakerTest() {
    }

    private CoffeeMaker cm;
    private Recipe r1;
    private Recipe r2;
    private Recipe r3;
    private Recipe r4;
    private Recipe errornousRecipe;

    @Before
    public void mySetUp() throws Exception {
        cm = new CoffeeMaker();

        //Set up for r1
        r1 = new Recipe();
        r1.setName("Coffee");
        r1.setAmtChocolate("0");
        r1.setAmtCoffee("3");
        r1.setAmtMilk("1");
        r1.setAmtSugar("1");
        r1.setPrice("50");

        //Set up for r2
        r2 = new Recipe();
        r2.setName("Mocha");
        r2.setAmtChocolate("20");
        r2.setAmtCoffee("3");
        r2.setAmtMilk("1");
        r2.setAmtSugar("1");
        r2.setPrice("75");

        //Set up for r3
        r3 = new Recipe();
        r3.setName("Latte");
        r3.setAmtChocolate("0");
        r3.setAmtCoffee("3");
        r3.setAmtMilk("3");
        r3.setAmtSugar("1");
        r3.setPrice("100");

        //Set up for r4
        r4 = new Recipe();
        r4.setName("Hot Chocolate");
        r4.setAmtChocolate("4");
        r4.setAmtCoffee("0");
        r4.setAmtMilk("1");
        r4.setAmtSugar("1");
        r4.setPrice("65");

    }

    @Test
    public void AddTooManyRecipes() throws InventoryException {
        assertTrue("r1 added succesfully", cm.addRecipe(r1));
        assertTrue("r2 added succesfully", cm.addRecipe(r2));
        assertTrue("r3 added succesfully", cm.addRecipe(r3));
        assertFalse("Only three recipes may be added to the CoffeeMaker.", cm.addRecipe(r4));
    }

    @Test
    public void AddUniqueRecipeName() throws InventoryException {

        assertTrue("r2 added succesfully", cm.addRecipe(r2));
        assertFalse("Each recipe name must be unique in the recipe list.", cm.addRecipe(r2));
    }

    @Test(expected = RecipeException.class)
    public void AddErrornousRecipe() throws InventoryException, RecipeException {
        errornousRecipe = new Recipe();
        errornousRecipe.setName("Hot Chocolate");
        errornousRecipe.setAmtChocolate("83");
        errornousRecipe.setAmtCoffee("0");
        errornousRecipe.setAmtMilk("1");
        errornousRecipe.setAmtSugar("1");
        errornousRecipe.setPrice("NULL");
    }

    @Test
    public void deleteRecipe() {
        assertTrue("add r1", cm.addRecipe(r1));
        assertTrue("r1 deletion", "Coffee".equals(cm.deleteRecipe(0)));
        assertFalse("r1 deletion again", "Coffee".equals(cm.deleteRecipe(0)));
    }

    @Test
    public void editRecipe() {
        assertNull("editing non existent tecipe", cm.editRecipe(0, r1));
        assertTrue("add r1", cm.addRecipe(r1));
//        System.out.println("!!!" + cm.getRecipes()[0].getName());
        assertNotNull("editing r1 to r2", cm.editRecipe(0, r2));
//        System.out.println("!!!" + cm.getRecipes()[0].getName());
        assertTrue("A recipe name may not be changed.", cm.getRecipes()[0].getName().equals("Coffee"));

    }

    @Test
    public void addInventoryIntParsing() {
        boolean success = true;
        try {
            cm.addInventory("2147483647", "2147483647", "2147483647", "2147483647");
        } catch (InventoryException ex) {
            System.out.println("addInventoryIntParsing: Problems with very large Ints");
            success = false;
        }
        try {
            cm.addInventory("1", "0", "0", "0");
        } catch (InventoryException ex) {
            System.out.println("addInventoryIntParsing: Problems with adding coffee");
            success = false;
        }
        try {
            cm.addInventory("0", "1", "0", "0");
        } catch (InventoryException ex) {
            System.out.println("addInventoryIntParsing: Problems with adding milk");
            success = false;
        }
        try {
            cm.addInventory("0", "0", "1", "0");
        } catch (InventoryException ex) {
            System.out.println("addInventoryIntParsing: Problems with adding sugar");
            success = false;
        }
        try {
            cm.addInventory("0", "0", "0", "1");
        } catch (InventoryException ex) {
            System.out.println("addInventoryIntParsing: Problems with adding chocolate");
            success = false;
        }
        if (!success) {
            fail("something went wrong: see log for details");
        }
    }

    @Test(expected = InventoryException.class)
    public void addInventoryException_Coffee() throws InventoryException {
        cm.addInventory("bla", "0", "7", "3");
    }

    @Test(expected = InventoryException.class)
    public void addInventoryException_Milk() throws InventoryException {
        cm.addInventory("0", "-1", "7", "3");
    }

    @Test(expected = InventoryException.class)
    public void addInventoryException_Sugar() throws InventoryException {
        cm.addInventory("0", "0", "bla7", "3");
    }

    @Test(expected = InventoryException.class)
    public void addInventoryException_Chocolate() throws InventoryException {
        cm.addInventory("0", "0", "7", "bla3");
    }

    @Test
    public void addInventory() {
        assertTrue("inventory check", cm.checkInventory().equals("Coffee: 15\nMilk: 15\nSugar: 15\nChocolate: 15\n"));
        try {
            cm.addInventory("4", "7", "0", "9");
        } catch (InventoryException e) {
            fail("InventoryException should not be thrown");
        }
        cm.addRecipe(r3);
        cm.addRecipe(r1);
        cm.addRecipe(r2);
        cm.deleteRecipe(0);
        cm.getRecipes()[0].getAmtMilk();
        assertTrue("inventory check", cm.checkInventory().equals("Coffee: 19\nMilk: 22\nSugar: 15\nChocolate: 24\n"));
        cm.makeCoffee(1, 150);
        assertFalse("inventory check", cm.checkInventory().equals("Coffee: 19\nMilk: 22\nSugar: 15\nChocolate: 24\n"));
    }

    @Test
    public void checkInventory() throws InventoryException {
        assertTrue("inventory check: initial values", cm.checkInventory().equals("Coffee: 15\nMilk: 15\nSugar: 15\nChocolate: 15\n"));
        cm.addInventory("4", "7", "0", "9");
        assertTrue("inventory check: after adding", cm.checkInventory().equals("Coffee: 19\nMilk: 22\nSugar: 15\nChocolate: 24\n"));
        cm.addRecipe(r1);
        cm.makeCoffee(0, 150);
        assertTrue("inventory check: after making coffee (wrong subtraction)", cm.checkInventory().equals("Coffee: 16\nMilk: 21\nSugar: 14\nChocolate: 24\n"));
    }

    @Test
    public void purchaseCoffee() {
        assertEquals("no recipe yet", 75, cm.makeCoffee(0, 75));
        cm.addRecipe(r1);
        assertEquals("normal purchase", 25, cm.makeCoffee(0, 75));
        assertEquals("0 money", 0, cm.makeCoffee(0, 0));
        assertEquals("not enough money", 30, cm.makeCoffee(0, 30));
        assertEquals("right amount of money", 0, cm.makeCoffee(0, 50));
        assertEquals("max int money", 2147483597, cm.makeCoffee(0, 2147483647));
        assertEquals("negativ money", -1, cm.makeCoffee(0, -1));
        //emptiing out the inventory
        for (int i = 0; i < 12; i++) {
            cm.makeCoffee(0, 75);
//            System.out.println(cm.checkInventory());
        }
        assertEquals("inventory empty", 60, cm.makeCoffee(0, 60));
    }
}
